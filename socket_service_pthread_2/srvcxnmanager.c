/**
 * \file srvcxnmanager.c
 * \brief Programme de  gestion fichier configuration
 * \author issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "srvcxnmanager.h"
#include "featureConfig.h"

connection_t* connections[MAXSIMULTANEOUSCLIENTS];
Player* players_array[MAXSIMULTANEOUSCLIENTS];
Duel* duels_array[MAXSIMULTANEOUSCLIENTS];


/**
 * \brief Fonction qui ainitialise un tableau de sockets
 */
void init_sockets_array() {
    for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
        connections[i] = NULL;
    }
}

/**
 * \brief Fonction qui initialise un tableau de joueurs
 */
void init_players_array() {
    for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
        players_array[i] = NULL;
    }
}

/**
 * \brief Fonction qui initialise un tableau de duels 
 */
void init_duels_array() {
    for (int i = 0; i < MAXSIMULTANEOUSDUELS; i++) {
        duels_array[i] = NULL;
    }
}


/**
 * \brief Fonction qui ajoute une connection dans le tableau
 * @param connection connection a ajouter dans le tableau
 */
void add(connection_t *connection) {
    for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
        if (connections[i] == NULL) {
            connections[i] = connection;
            return;
        }
    }
    perror("Too much simultaneous connections");
    exit(-5);
}

/**
 * fonction qui ajoute un joueur dans le tableau
 * @param player structure du joueur à ajouter
 */
void add_player(Player *player) {
    for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
        if (players_array[i] == NULL) {
            players_array[i] = player;
            players_array[i]->index = i;
            players_array[i]->status = 0;
            return;
        }
    }
    perror("Too much simultaneous connections");
    exit(-5);
}

/**
 * fonction ajoute un duel dans le tableau de duel
 * @param duel structure duel
 */
void add_duel(Duel *duel) {
    for (int i = 0; i < MAXSIMULTANEOUSDUELS; i++) {
        if (duels_array[i] == NULL) {
            duels_array[i] = duel;
            duels_array[i]->index = i;
            return;
        }
    }
    perror("Too much simultaneous connections");
    exit(-5);
}

/**
 * \brief fonction qui supprime une connection dans le tableau
 * @param connection
 */
void del(connection_t *connection) {
    for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
        if (connections[i] == connection) {
            connections[i] = NULL;
            return;
        }
    }
    perror("Connection not in pool ");
    exit(-5);
}
 /**
  * fonction qui supprime un joueur du tableau
  * @param player
  */
void del_player(Player *player) {
    for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
        if (players_array[i] == player) {
            players_array[i] = NULL;
            return;
        }
    }
    perror("Connection not in pool ");
    exit(-5);
}

/**
 * fonction qui supprime un duel du tableau
 * @param duel
 */
void del_duel(Duel *duel) {
    for (int i = 0; i < MAXSIMULTANEOUSDUELS; i++) {
        if (duels_array[i] == duel) {
            duels_array[i] = NULL;
            return;
        }
    }
    perror("Connection not in pool ");
    exit(-5);
}

/**
 * fonction de recherche d'adversaire
 * @param connection
 * @param player
 */
void rechercheAdv(connection_t *connection, Player *player) {

    player->status = 1;
    DuelConfig duelConfig = readConfig();
    while(1){
        for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
            if (players_array[i] != NULL 
                    && i != player->index 
                    && (strncmp(players_array[i]->KEY, player->KEY, strlen(player->KEY)))==0)
            {
                printf("Adversaire trouver\n");
                Duel *duel = malloc(sizeof(Duel));
                strcpy(duel->player1, player->NAME);
                strcpy(duel->player2, players_array[i]->NAME);
                strcpy(duel->key, players_array[i]->KEY);
                duel->mise = duelConfig.mise;
                duel->round = duelConfig.round;
                duel->reponse = 0;
                add_duel(duel);
                return;
            }
        }
    }
    
}
/*
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_lock(&lock);
pthread_mutex_unlock(&lock);
 */

/**
 * Thread allowing server to handle multiple client connections
 * @param ptr connection_t 
 * @return 
 */
void *threadProcess(void *ptr) {
    char buffer_in[BUFFERSIZE];
    char buffer_out[BUFFERSIZE];

    int len;
    int currentRound = 1, endRound, nbRound;
    
    connection_t *connection;
    Player *player = malloc(sizeof(Player));

    if (!ptr) pthread_exit(0);
    connection = (connection_t *) ptr;
    printf("New incoming connection \n");

    add(connection);

    //Welcome the new client
    printf("Welcome #%i\n", connection->index);
    
    while ((len = read(connection->sockfd, buffer_in, BUFFERSIZE)) > 0) {
        
        int *typePacket = buffer_in;
        
        if(*typePacket==1){
            
            Message *msg = malloc(sizeof(Message));
            
            memcpy(player, buffer_in, sizeof(Player));
            
            
            add_player(player);
            
            msg->typePacket=2;
            strcpy(msg->label, "Connection success! Searching for opponent\n");
            write(connection->sockfd, msg, sizeof(Message));
            

            //Création du duel
            for (int i = 0; i < MAXSIMULTANEOUSCLIENTS; i++) {
                if(players_array[i] != NULL){
                    //Si les joueurs ont la meme clé
                    if(players_array[i]->status == 1 
                            && strncmp(players_array[i]->KEY, players_array[player->index]->KEY, strlen(players_array[player->index]->KEY)) ==0 
                            && players_array[i]->index != players_array[player->index]->index)
                    {
                        players_array[player->index]->status = 1;
                        break;
                    } else if(strncmp(players_array[i]->KEY, players_array[player->index]->KEY, strlen(players_array[player->index]->KEY)) !=0){
                        printf("Pas le bon ennemi\n");
                    } else {
                        rechercheAdv(connection, players_array[player->index]);
                        break;
                    }
                }
            }

            
            msg->typePacket=3;
            strcpy(msg->label, "Opponent founded!\n");
            write(connection->sockfd, msg, sizeof(Message));

            //clear input buffer
            memset(buffer_in, '\0', BUFFERSIZE);
        }
        // message 4 reponse au client , lancement de la partie
        if(*typePacket==4){
            
            Message *msg = malloc(sizeof(Message));
            GameDecision *decision = malloc(sizeof(GameDecision));
            endRound = 0;
            
            int indexOtherClient = -1; // Déclaration d'index pour récupéré un client
            
            for(int i = 0; i<MAXSIMULTANEOUSDUELS; i++){
                if(strncmp(players_array[player->index]->KEY,duels_array[i]->key, strlen(players_array[player->index]->KEY)) == 0){
                    
                    duels_array[i]->reponse++;
                    nbRound = duels_array[i]->round;
                    
                    if(duels_array[i]->reponse < 2){
                        if(strncmp(players_array[player->index]->NAME,duels_array[i]->player1, strlen(players_array[player->index]->NAME)) == 0){
                            memcpy(decision, buffer_in, sizeof(GameDecision));
                            
                        } else if (strncmp(players_array[player->index]->NAME,duels_array[i]->player2, strlen(players_array[player->index]->NAME)) == 0){
                            memcpy(decision, buffer_in, sizeof(GameDecision));
                            if(decision->choice == 1){
                                decision->choice++;
                            }
                        }
                        duels_array[i]->resultat[currentRound-1] += decision->choice;
                        msg->typePacket= 5;
                        strcpy(msg->label, "Choix reçu! En attente de la réponse du deuxième joueur.\n");
                        currentRound++;
                        break;
                    }
                    else{
                        msg->typePacket= 6;
                        
                        if(strncmp(players_array[player->index]->NAME,duels_array[i]->player1, strlen(players_array[player->index]->NAME)) == 0){
                            memcpy(decision, buffer_in, sizeof(GameDecision));
                            for(int j = 0; j<MAXSIMULTANEOUSCLIENTS; j++){
                                if(players_array[j] != NULL){
                                    if(strncmp(duels_array[j]->player2,players_array[j]->NAME, strlen(duels_array[j]->player2)) == 0){
                                        indexOtherClient = players_array[j]->index;
                                        break;
                                    }
                                }
                            }
                        } else if (strncmp(players_array[player->index]->NAME,duels_array[i]->player2, strlen(players_array[player->index]->NAME)) == 0){
                            memcpy(decision, buffer_in, sizeof(GameDecision));
                            if(decision->choice == 1){
//                                printf("choix2 : %d\n", decision->decision);
                                decision->choice++;
                            }
                            for(int j = 0; j<MAXSIMULTANEOUSCLIENTS; j++){
                                if(players_array[j] != NULL){
                                    if(players_array[j] != NULL){
                                        if(strncmp(duels_array[i]->player1,players_array[j]->NAME, strlen(duels_array[i]->player1)) == 0){
                                            indexOtherClient = players_array[j]->index;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        duels_array[i]->resultat[currentRound-1] += decision->choice;
                        msg->status= duels_array[i]->resultat[currentRound-1];
                        
                        // gestion des resultats
                        if(msg->status == 0){
                            strcpy(msg->label, "La cagnotte a été partagée!\n");
                        } else if(msg->status == 1){
                            strcpy(msg->label, "Le joueur ");
                            strcat(msg->label, duels_array[i]->player1);
                            strcat(msg->label, " a volé ");
                            strcat(msg->label, duels_array[i]->player2);
                            strcat(msg->label, " !\n");

                        } else if(msg->status == 2){
                            strcpy(msg->label, "Le joueur ");
                            strcat(msg->label, duels_array[i]->player2);
                            strcat(msg->label, " a volé ");
                            strcat(msg->label, duels_array[i]->player1);
                            strcat(msg->label, " !\n");

                        } else if(msg->status == 3){
                            strcpy(msg->label, "L'adversaire a aussi tenté de vous voler, la cagnotte est perdue!\n");
                        }
                        
                        currentRound++;
                        endRound++;
                        duels_array[i]->reponse = 0;

                        break;
                    }
                    
                }
            
            }
            
            //reponse aux clients
            write(connection->sockfd, msg, sizeof(Message));
            
            sleep(1);
            if(indexOtherClient != -1){
                if(connections[indexOtherClient]->sockfd != connection->sockfd){
                    write(connections[indexOtherClient]->sockfd, msg, sizeof(Message));
                }
            }
            
            sleep(1);
            
            if(endRound == 1){
                if(currentRound-1 < nbRound){
                    msg->typePacket=7;
                    sprintf(msg->label, "%d", nbRound-currentRound+1);
                    strcat(msg->label, " Round left!\n");
                    msg->status = currentRound;
                    write(connection->sockfd, msg, sizeof(Message));
                    sleep(1);
                    if(indexOtherClient != -1){
                        if(connections[indexOtherClient]->sockfd != connection->sockfd){
                            write(connections[indexOtherClient]->sockfd, msg, sizeof(Message));
                        }
                    }
                }
                else{
                    msg->typePacket=8;
                    strcpy(msg->label, "Game Over!\n");
                    sleep(1);
                    write(connection->sockfd, msg, sizeof(Message));

                    sleep(1);

                    //Envoi des deux structures players contenant les résultats des jeux
                    msg->typePacket=9;
                    strcpy(msg->label, "Envoi des résultats!\n");
                    msg->status = currentRound;
                    write(connection->sockfd, msg, sizeof(Message));
                    sleep(1);
                    if(indexOtherClient != -1){
                        if(connections[indexOtherClient]->sockfd != connection->sockfd){
                            write(connections[indexOtherClient]->sockfd, msg, sizeof(Message));
                        }
                    }
                }
            }
            
            memset(buffer_in, '\0', BUFFERSIZE);
            
        }
        
    }
    printf("Connection to client %i ended \n", connection->index);
    close(connection->sockfd);
    del(connection);
    del_player(player);
    free(connection);
    pthread_exit(0);

}

int create_server_socket() {
    int sockfd = -1;
    struct sockaddr_in address;
    int port = 7799;

    /* create socket */
    sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd <= 0) {
        fprintf(stderr, "%s: error: cannot create socket\n");
        return -3;
    }


    /* bind socket to port */
    address.sin_family = AF_INET;
    //bind to all ip : 
    //address.sin_addr.s_addr = INADDR_ANY;
    //ou 0.0.0.0 
    //Sinon  127.0.0.1
    address.sin_addr.s_addr = inet_addr("0.0.0.0");
    address.sin_port = htons(port);

    /* prevent the 60 secs timeout */
    int reuse = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char*) &reuse, sizeof (reuse));

    /* bind */
    if (bind(sockfd, (struct sockaddr *) &address, sizeof (struct sockaddr_in)) < 0) {
        fprintf(stderr, "error: cannot bind socket to port %d\n", port);
        return -4;
    }

    return sockfd;
}
