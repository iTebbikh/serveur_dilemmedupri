/**
 * \file featureConfig.h
 * \brief Structures pour les fichiers de configurations
 * \author issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 *
 */

#ifndef FEATURECONFIG_H
#define FEATURECONFIG_H

/**
 * \struct DuelConfig
 */
typedef struct {
    int round;
    int mise;

} DuelConfig;


/**
 * \struct ToWrite
 */
typedef struct {
    char nom[16];
    char prenom[5];
    int score;

} ToWrite;


DuelConfig readConfig();
void writeResult(ToWrite toWrite);

#endif /* FEATURECONFIG_H */

