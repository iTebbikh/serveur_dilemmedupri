/**
 * \file srvcxnmanager.h
 * \brief structure pour communiquer avec les clients
 * \author issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 *
 */

#ifndef SRVCXNMANAGER_H
#define SRVCXNMANAGER_H

#define BUFFERSIZE 2048
#define MAXSIMULTANEOUSCLIENTS 100
#define MAXSIMULTANEOUSDUELS 50


typedef struct {
    int sockfd;
    struct sockaddr address;
    int addr_len;
    int index;
} connection_t;


/**
 * \struct Message
 * \brief message pour comuniquer avec le client
 */
typedef struct {
    int typePacket; // Savoir le type du message (start ou getready...)
    char label[120];
    int status; // Message reçu ou pas
     
}Message;


/**
 * \struct GameDecision
 * \brief structure pour rendre le choix des joueurs
 */
typedef struct {
    int typePacket; // Savoir le type du message (start ou getready...)
    int choice; // 
     
}GameDecision;


/**
 * \struct Player
 * \brief structure qui contient toutes les données des joueurs
 */
typedef struct {
    int typePacket;
    char NAME[16];
    char IP[16];
    char KEY[10];
    int PORT;
    int score;
    int choix;
    int status; //1=en jeu , 0=pas en jeu
    int index;

} Player;

/**
 * \struct Duel
 * \brief structure qui contient toutes les données sur les duels
 */
typedef struct {
    char player1[16];
    char player2[16];
    int mise;
    int round;
    char key[10];
    int resultat[100];
    int reponse;
    int index;
} Duel;

void init_sockets_array();
void init_players_array();

void add(connection_t *connection);
void add_player(Player *player);
void del(connection_t *connection);
void del_player(Player *player);
void *threadProcess(void *ptr);
int create_server_socket() ;

#endif /* SRVCXNMANAGER_H */

