/**
 * \file featureConfig.c
 * \brief Programme de  gestion fichier configuration
 * \author issam TEBBIKH, thomas ROSSI
 * \version 0.1
 * \date 20/12/2019

 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "featureConfig.h"


/**
 * Fonction qui recupere les paramèrtes du fichier de configuration du serveur
 * @return 
 */
DuelConfig readConfig() {
    DuelConfig config;
    
    FILE* fichier = NULL;
    fichier = fopen("config.txt", "r");

    char ligne[30];
    char delim[] = "=";

    for (int i = 0; i < 2; i++) {
        fscanf(fichier, "%s", ligne);
        char *ptr = strtok(ligne, delim);

        if (strcmp(ptr, "ROUND") == 0) {
            ptr = strtok(NULL, delim);
            config.round=atoi(ptr);
        } else if (strcmp(ptr, "MISE") == 0) {
            ptr = strtok(NULL, delim);
            config.mise=atoi(ptr);
        }


    }
    fclose(fichier);
    return config;
}

/**
 * Fonction qui écris dans le fichier de log les résultats
 * @param toWrite
 */
void writeResult(ToWrite toWrite) {
    FILE *fichier = fopen("write_file.txt", "w");
    fprintf(fichier, "%s=%s", "Nom", toWrite.nom);
    fputs("\n",fichier);
    fprintf(fichier, "%s=%s", "Prenom", toWrite.prenom);
    fputs("\n",fichier);
    fprintf(fichier, "%s=%i", "Score", toWrite.score);
    fputs("\n",fichier);

    fclose(fichier);
}
//int main() {
//    Config config = readConfig();
//    printf("L'IP est %s\n", config.IP);
//    printf("Le PORT est %s\n", config.PORT);
//    printf("La clée est %s\n", config.KEY);
//    
//    ToWrite toWrite;
//    strcpy(toWrite.nom , "ROSSI");
//    strcpy(toWrite.prenom , "Thomas");
//    toWrite.score = 13;
//    write(toWrite);
//}