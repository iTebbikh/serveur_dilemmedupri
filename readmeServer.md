# Tutoriel Dilèmme Prisonnier

Ce guide est une documentation décrivant la liste des étapes nécessaire à réaliser afin de déployer correctement l'application de duel

# Sommaire
1. [Présentation de l'application](#InfoApp)
2. [Installation de l'application](#Install)
3. [Informations relative au fonctionnement](#InfoWork)
4. [Liste des fichiers crées/utilisés par l'application](#ListFile)
5. [Configuration de l'application](#InfoConfig)

## Présentation de l'application <a id="InfoApp"></a>

### Présentation du logiciel :
Le logiciel permet d’étudier la nature et la spécificité de l'esprit humain.

Il va permettre au doctorant de travailler sur la théorie des jeux. Pour cela, on utilisera le jeu du dilemme du prisonnier afin d’avoir des résultats.

Ce logiciel met en relation 2 joueurs qui s’affrontent à travers leurs ordinateurs, les joueurs auront 2 choix : VOLER ou PARTAGER.

* Cas 1 : Les 2 joueurs choisissent voler
Résultat : Aucun des joueurs gagnera le gain

* Cas 2 : Les 2 joueurs choisissent partager
Résultat : Il partage le gain

* Cas 3 : Un joueur choisit partager et un autre voler
Résultat : Le joueur qui choisit voler gagne tout le gain

## Installation de l'application <a id="Install"></a>

## Informations relative au fonctionnement <a id="InfoWork"></a>


Cette application nécessite un seul serveur pour fonctionner, en effet tout les clients sont connecté au même serveur en même temps.
Cette architecture centralisée permet de définir des règles uniformes, une architecture plus simple et de centraliser les journaux.

Informations de base à noter pour la configuration :

1. Adresse IP : IP du serveur a ajouté sur les fichiers de conf des clients (nom de domaine pas encore supporté)
2. Port distant : Port de connexion au serveur, peut être changer dans le serveur (à adapté en fonction de votre réseau/par-feux)

## Liste des fichiers crées/utilisés par l'application <a id="ListFile"></a>

* Serveur

        Exécutable : exe_srv_deleme_prisonier
        Configuration : config_srv_deleme_prisonier.ini
        Logs : log_srv_deleme_prisonier.log

    L'exécutable est le fichier à executer au démarrage du jeu, avant tout les clients.

    Le fichier de configuration est lu au démarrage de l'application pour charger la configuration (somme engagée, nombre de parties).

    Le fichier de log rassemble tout les logs des client en un seul fichier sur le serveur.

    ## Configuration de l'application <a id="InfoConfig"></a>

    Les fichiers de configuration sont dans le même répertoire que l'exécutable

    Le format est en .ini donc clé et valeur avec le délimiteur "="

* Serveur

    Nom du fichier : ```config_srv_deleme_prisonier.ini```

    ### Exemple de la configuration minimun :

    ```
    IP=192.168.0.250
    PORT=7799
    ```

    ## Toutes les options :
    ```
    IP
    PORT
    ```